# CI/CD Templates

Pipeline templates used by Applifting Cloud Engineering

## Description

The CI/CD Templates repository serves the purpose of providing a variety of options how to implement GitLab CI/CD for your specific use case.

## Contents

| Templates  | Base |
| ------------- | ------------- |
| [Terraform.latest](Terraform.latest.gitlab-ci.yml)  | [Terraform/Base.latest](Terraform/Base.latest.gitlab-ci.yml)  |
| [Terraform](Terraform.gitlab-ci.yml)  | [Terraform/Base](Terraform/Base.gitlab-ci.yml)  |
| [Terraform-Module-Upload-Simple](Terraform-Module-Upload-Simple.gitlab-ci.yml)  | [Terraform/Module-Upload/Base](Terraform/Module-Upload/Base.gitlab-ci.yml)  |
| [Terraform-Module-Upload-Versioning](Terraform-Module-Upload-Versioning.gitlab-ci.yml)  | [Terraform/Module-Upload/Base](Terraform/Module-Upload/Base.gitlab-ci.yml) <br> [GitVersion/Base](GitVersion/Base.gitlab-ci.yml)  |
| [Terraform-Gcloud-Auth](Terraform-Gcloud-Auth.gitlab-ci.yml)  | [Terraform/Base.latest](Terraform/Base.latest.gitlab-ci.yml) <br> [Authentication/Gcloud](Authentication/Gcloud.gitlab-ci.yml)  |


## Example usage

### [Terraform-Module-Upload-Versioning](Terraform-Module-Upload-Versioning.gitlab-ci.yml)
```yaml 
include:
  - project: 'applifting-cloud-engineering/cicd-templates'
    ref: main
    file: 'Terraform-Module-Upload-Versioning.gitlab-ci.yml'

variables:
  TERRAFORM_MODULE_DIR: modules/rbac  # path to your Terraform module
```
### [Terraform-Gcloud-Auth](Terraform-Gcloud-Auth.gitlab-ci.yml)
```yaml
include:
  - project: 'applifting-cloud-engineering/cicd-templates'
    ref: main
    file: 'Terraform-Gcloud-Auth.gitlab-ci.yml'

variables:
  GCLOUD_PROJECT: "gcloud_project_number"                                           # Project number within the Google Cloud Provider account
  GCLOUD_WORKLOAD_IDENTITY_POOL: "gitlab"                                           # The full identifier of the Workload Identity Pool
  GCLOUD_WORKLOAD_IDENTITY_PROVIDER: "my_gitlab_group"                              # The full identifier of the Workload Identity Provider
  GCLOUD_SERVICE_ACCOUNT: "<service_account>@<project_id>.iam.gserviceaccount.com"  # Authorize access to Google Cloud with a service account  
```

### [Terraform/Base.latest.gitlab-ci.yml](Terraform/Base.latest.gitlab-ci.yml)
```yaml
include:
  - project: 'applifting-cloud-engineering/cicd-templates'
    ref: main
    file: 'Terraform/Base.latest.gitlab-ci.yml'

stages:
  - plan
  - apply
  - state-unlock
  - destroy-plan
  - destroy-apply

plan:
  extends: 
    - .plan 

apply:
  extends: 
    - .apply
  dependencies:
    - plan
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
    - when: never

state-unlock:
  extends: 
    - .state-unlock

destroy-plan:
  extends: 
    - .destroy-plan
  
destroy-apply:
  extends: 
    - .destroy-apply
```
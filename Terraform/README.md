## Controlling the Terraform behaviour

Pipeline can be run with several variables which instructs Terraform for some specific actions:

- `TF_INIT_ARG`: Terraform init arguments, for example `-upgrade` or `-reconfigure`.
- `TF_PLAN_TARGET`: Runs a `terraform plan` for a specific resource. Several targets can be specified, separated by `;`.
- `TF_STATE_LOCK_ID`: Force unlocks the locked terraform state with the specified lock-id.
- `TF_DESTROY`: Runs `terraform destroy` for the whole infrastructure. Can be combined with `TF_PLAN_TARGET` variable. 

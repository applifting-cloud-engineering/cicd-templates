# Google Cloud Provider authentication

Authentication within Google Cloud Provider can be used as an optional feature in a pipeline. 

Authentication uses Workload Identity Federation which is a keyless application authentication mechanism in Google Cloud. It follows the OAuth 2.0 token exchange protocol.

## Requirements

 - [Google Cloud Workload Identity Federation](https://cloud.google.com/iam/docs/workload-identity-federation) needs to be set up with a provider configured for your group/repository.

## How to use it in your project
You can copy the template directly into your `gitlab-ci.yml` file or you can <br>
Include the template in your GitLab CI/CD configuration. For example:

```yaml
include:
  - project: applifting-cloud-engineering/cicd-templates
    ref: master
    file: Authentication/Gcloud.gitlab-ci.yml
```
### Caution:
`.gcloud_auth` is defined as a `before_script`. Keep that in mind while using the template as it may run into issues with `before_scripts` defined in your `gitlab-ci.yml` file
## Requiered variables

If you want to enable GCP authentication, set the following variables

```yaml
variables:
  GCLOUD_PROJECT: "<gcloud_project_number>"                                         # Project number within the Google Cloud Provider account
  GCLOUD_WORKLOAD_IDENTITY_POOL: "gitlab"                                           # The full identifier of the Workload Identity Pool
  GCLOUD_WORKLOAD_IDENTITY_PROVIDER: "my_gitlab_group"                              # The full identifier of the Workload Identity Provider
  GCLOUD_SERVICE_ACCOUNT: "<service_account>@<project_id>.iam.gserviceaccount.com"  # Authorize access to Google Cloud with a service account  
```

## Jobs 

Define the following jobs, extending the template jobs, for example:

```yaml
job_1:
  stage: stage_1
  extends: 
    - .gcloud_auth
```
# GitVersion



## Requirements

 - A CI/CD variable named `CICD_TOKEN` is saved in the project CI/CD settings. The value should be an access token with API scope

## How to use it in your project
You can copy the template directly into your `gitlab-ci.yml` file or you can <br>
Include the template in your GitLab CI/CD configuration. For example:

```yaml
include:
  - project: applifting-cloud-engineering/cicd-templates
    ref: master
    file: GitVersion/Base.gitlab-ci.yml
```

## Jobs 

Define the following jobs, extending the template jobs, for example:

```yaml
job_1:
  stage: stage_1
  extends: 
    - .gcloud_auth
```

<!-- ## Requiered variables

If you want to enable GCP authentication, set the following variables

```yaml
variables:
  GCLOUD_PROJECT: "<gcloud_project_number>"                                         # Project number within the Google Cloud Provider account
  GCLOUD_WORKLOAD_IDENTITY_POOL: "gitlab"                                           # The full identifier of the Workload Identity Pool
  GCLOUD_WORKLOAD_IDENTITY_PROVIDER: "my_gitlab_group"                              # The full identifier of the Workload Identity Provider
  GCLOUD_SERVICE_ACCOUNT: "<service_account>@<project_id>.iam.gserviceaccount.com"  # Authorize access to Google Cloud with a service account  
``` -->